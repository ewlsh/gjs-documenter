import fs from 'fs';
import { v4 as uuid } from 'uuid';
import util from 'util';
import glob from 'fast-glob';

import type { NamespaceJson } from '@gi.ts/lib/dist/generators/json';

const readFile = util.promisify(fs.readFile);

async function createNodeOptions(file: string): Promise<[NamespaceJson, {}]> {
  const content = await readFile(file, 'utf8');

  const data = JSON.parse(content) as NamespaceJson;
  return [data, {}];
}

async function getFiles(path: string): Promise<string[]> {
  return await glob(path);
}

async function loadNamespaces(): Promise<NamespaceJson[]> {
  const namespacePath = './json/*.json';

  console.log(`Loading namespaces from ${namespacePath}...`);
  const files = await getFiles(namespacePath);

  const namespaces = [];
  for (let file of files) {
    const [data, meta] = await createNodeOptions(file);

    if (!data) {
      console.log(`Skipping ${file} due to generation failure.`);
      continue;
    }

    const nm = `${data.name.toLowerCase()}-${data.version}`;

    const _constants = data.constants.map((c) => {
      const id = uuid();

      return {
        id,
        path: `/${nm}/${c.name}`,

        ...c,
      };
    });

    const paths = new Map();

    /**
     * @param {any} name
     */
    function makePath(name: string) {
      const path = `/${nm}/${name}`;

      paths.set(name, path);

      return path;
    }

    function hasValidPath(name: string) {
      return !paths.has(makePath(name));
    }

    const function_map = new Map();

    data.functions.forEach((f) => {
      if (!function_map.has(f.name)) {
        function_map.set(f.name, []);
      }

      function_map.get(f.name).push(f);
    });

    const _functions = Array.from(function_map.entries())
      .map(([name, funcs]) => {
        const id = uuid();

        if (!hasValidPath(name)) {
          return null;
        }

        return {
          id,
          path: makePath(name),

          ...funcs[0],
          overrides: funcs.splice(1),
        };
      })
      .flatMap((a) => a ?? []);
    const _classes = data.classes
      .map((c) => {
        const id = uuid();

        if (!hasValidPath(c.name)) {
          return null;
        }

        return {
          id,
          path: makePath(c.name),

          ...c,
        };
      })
      .flatMap((a) => a ?? []);

    const _records = data.records
      .map((r) => {
        const id = uuid();

        if (!hasValidPath(r.name)) {
          return null;
        }

        return {
          id,
          path: makePath(r.name),

          ...r,
        };
      })
      .flatMap((a) => a ?? []);

    const _interfaces = data.interfaces
      .map((i) => {
        const id = uuid();

        if (!hasValidPath(i.name)) {
          return null;
        }

        return {
          id,
          path: makePath(i.name),

          ...i,
        };
      })
      .flatMap((a) => a ?? []);

    const _enums = data.enums
      .map((e) => {
        const id = uuid();

        if (!hasValidPath(e.name)) {
          return null;
        }

        return {
          id,
          path: makePath(e.name),

          ...e,
        };
      })
      .flatMap((a) => a ?? []);
    const _errors = data.errors
      .map((e) => {
        const id = uuid();

        if (!hasValidPath(e.name)) {
          return null;
        }

        return {
          id,
          path: makePath(e.name),

          ...e,
        };
      })
      .flatMap((a) => a ?? []);

    const _alias = data.alias
      .map((e) => {
        const id = uuid();

        if (!hasValidPath(e.name)) {
          return null;
        }

        return {
          id,
          path: makePath(e.name),

          ...e,
        };
      })
      .flatMap((a) => a ?? []);

    const reffedData = { ...data };
    reffedData.constants = _constants;
    reffedData.functions = _functions;
    reffedData.classes = _classes;
    reffedData.records = _records;
    reffedData.interfaces = _interfaces;
    reffedData.enums = _enums;
    reffedData.errors = _errors;
    reffedData.alias = _alias;

    const x = {
      ...meta,
      ...reffedData,
      path: `/${nm}`,
    };

    namespaces.push(x);
  }

  console.log(`Loaded ${namespaces.length} namespaces...`);

  return namespaces;
}

export { loadNamespaces };
