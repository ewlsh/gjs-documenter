import React from 'react';

export const ClientSide: React.FC<{
  serverSide?: JSX.Element | null;
  children: () => JSX.Element | null;
}> = ({ children, serverSide = null }) => {
  if (typeof window === 'undefined') {
    return serverSide;
  }

  return <React.Suspense fallback={serverSide}>{children()}</React.Suspense>;
};
