import React from 'react';
import { useNamespaces } from '../hooks/useNamespaces';

import './Sidebar.scss';

const Sidebar: React.FC = ({}) => {
  const namespaces = useNamespaces();

  const namespaceList = Object.entries(namespaces);

  return (
    <ul className="sidebar">
      {namespaceList.map(([namespace, versions]) => {
        if (versions.length == 1) {
          const [{ version }] = versions;

          return (
            <li key={`sidebar-entry-${namespace}`}>
              <a href={`/${namespace.toLowerCase()}-${version}/`}>
                {namespace} {version}
              </a>
            </li>
          );
        }

        return (
          <li key={`sidebar-entry-${namespace}`}>
            <details>
              <summary>{namespace}</summary>
              <ul className="version-list">
                {versions.map(({ name: namespace, version }) => {
                  return (
                    <li key={`sidebar-subentry-${namespace}-${version}`}>
                      <a href={`/${namespace.toLowerCase()}-${version}/`}>
                        {namespace} {version}
                      </a>
                    </li>
                  );
                })}
              </ul>
            </details>
          </li>
        );
      })}
    </ul>
  );
};

export default Sidebar;
