import React from 'react';

interface AutocompleteProps {
  hits: any[];
  currentRefinement: string;
  refine?: (refinement: string) => void;
}

export const Autocomplete: React.FC<AutocompleteProps> = ({
  hits,
  currentRefinement,
  refine,
}) => {
  return (
    <div className="search-container">
      <input
        className={`search${
          currentRefinement && hits.length > 0 ? ' open' : ''
        }`}
        type="search"
        value={currentRefinement}
        disabled={!refine}
        onChange={(event) => refine?.(event.currentTarget.value)}
      />
      <ul
        className={`search-results${
          currentRefinement && hits.length > 0 ? ' open' : ''
        }`}
      >
        {currentRefinement ? (
          hits.map((hit) => {
            const { name, namespace, version } = hit;

            return (
              <li key={hit.id} className="search-result">
                <a
                  onClick={() => refine?.('')}
                  href={`${hit.path}/`}
                  title={name}
                >
                  {namespace ? (
                    <>
                      <span className="search-highlight">
                        {namespace} {version}
                      </span>{' '}
                      ❯ {name}
                    </>
                  ) : (
                    `${name} ❯ ${version}`
                  )}
                </a>
              </li>
            );
          })
        ) : (
          <></>
        )}
      </ul>
    </div>
  );
};
