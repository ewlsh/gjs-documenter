import React from 'react';

export const ServerSide: React.FC = ({ children }) => {
  if (typeof window === 'undefined') {
    return (
        <div>{children}</div>
    );
  }

  return (
    <div
      dangerouslySetInnerHTML={{
        __html: '',
      }}
      suppressHydrationWarning
    />
  );
};

export function serverSide<T>(cb?: (...args: any[]) => T): T | undefined {
  return typeof window === 'undefined' ? cb?.() : undefined;
}
