import React from 'react';
import { Autocomplete } from './Autocomplete';
import './Search.scss';

export const Search: React.FC<{ createWorker: () => SharedWorker }> = ({
  createWorker,
}) => {
  if (typeof window === 'undefined') {
    return <Autocomplete hits={[]} currentRefinement="" />;
  }

  const AutocompleteLazy = React.lazy(
    () => import('./AutocompleteWorkerConnection')
  );

  return (
    <React.Suspense
      fallback={
        <Autocomplete hits={[]} currentRefinement="" />
      }
    >
      <AutocompleteLazy createWorker={createWorker} />
    </React.Suspense>
  );
};

export default Search;
