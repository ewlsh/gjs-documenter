import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Autocomplete } from './Autocomplete';

const AutocompleteAdapter: React.FC<{
  createWorker: () => SharedWorker;
}> = ({ createWorker }) => {
  const [currentRefinement, setCurrentRefinement] = useState('');
  const [results, setResults] = useState([]);
  const refine = useCallback((search: string) => {
    worker.current?.port.postMessage(search);
  }, []);
  const worker = useRef<SharedWorker>();

  useEffect(() => {
    if (currentRefinement) refine(currentRefinement);
  }, [currentRefinement]);

  if (!worker.current) {
    worker.current = createWorker();

    worker.current.port.onmessage = function (event) {
      try {
        console.log(event.data);
        // @ts-ignore
        setResults([...event.data]);
      } catch (error) {
        console.log(error);
      }
    };

    worker.current.port.onmessageerror = function (error) {
      console.log(error);
    };

    worker.current.port.start();
  }

  return (
    <Autocomplete
      refine={setCurrentRefinement}
      hits={results}
      currentRefinement={currentRefinement}
    />
  );
};

export default AutocompleteAdapter;