import * as fs from 'node:fs';
import * as flexsearch from './flexsearch';

import { loadNamespaces } from '../routes';
import { NamespaceJson } from '@gi.ts/lib/dist/generators/json';
import { optimize, toDocuments, defaultSearchOptions } from './search';

declare module globalThis {
  export let allNamespaces: NamespaceJson[];
  export let searchUpdated: {};
  export let searchUpdating: boolean;
}

export async function loadAllNamespaces(): ReturnType<typeof loadNamespaces> {
  if (!globalThis.allNamespaces)
    globalThis.allNamespaces = (await loadNamespaces()).map((n) => ({
      slug: `${n.name}-${n.version}`.toLowerCase(),
      ...n,
    }));
  return globalThis.allNamespaces;
}

export async function updateSearchData() {
  if (globalThis.searchUpdating) return;

  globalThis.searchUpdating = true;

  const namespaces = await loadAllNamespaces();

  if (namespaces && globalThis.searchUpdated === namespaces) return;

  globalThis.searchUpdated = namespaces;
  globalThis.searchUpdating = false;

  const document = new flexsearch.Document<unknown, string[]>({
    ...defaultSearchOptions,
  });

  console.log(`Updating search index...`);

  const objects = namespaces.map((namespace) => {
    return {
      name: namespace.name,
      version: namespace.version,
      path: `/${namespace.name.toLowerCase()}-${namespace.version}`,
      documents: {
        functions: namespace.functions.map((func) => {
          return {
            name: func.name,
            path: `/${func.name}`,
          };
        }),
        class: [
          ...namespace.classes,
          ...namespace.interfaces,
          ...namespace.records,
        ].map((cl) => {
          return {
            name: cl.name,
            path: `/${cl.name}`,
            children: [
              ...cl.methods.map((m) => m.name),
              ...cl.staticMethods.map((m) => m.name),
              ...cl.virtualMethods.map((m) => m.name),
              ...cl.props.map((p) => p.name),
            ],
          };
        }),
        constant: namespace.constants.map((constant) => {
          return {
            name: constant.name,
            path: `/${constant.name}`,
          };
        }),
      },
    };
  });

  const optimized = optimize(objects);

  toDocuments(optimized).forEach((doc) => document.add(doc.path, doc));

  console.log('Search Updated!');

  await exportSearchData(document, optimized);

  console.log('Search exported!');
}

const exportSearchData = async <T>(
  document: flexsearch.Document<T, string[]>,
  objects: any[] = []
) => {
  const output: Record<string, {}> = {};
  await document.export(function (key, data) {
    output[key] = data;
  });

  output['::documents'] = JSON.stringify(objects);

  await new Promise<void>((resolve, reject) =>
    fs.writeFile(
      './public/search.json',
      JSON.stringify(output, null, 4),
      (err) => {
        if (err) reject(err);
        resolve();
      }
    )
  );
};
