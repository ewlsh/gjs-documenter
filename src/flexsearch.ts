import type * as Flexsearch from '../node_modules/@types/flexsearch/index';
import type {
  IndexOptionsForDocumentSearch,
  Document as TDocument,
} from '../node_modules/@types/flexsearch/index';

import * as _flexsearch from 'flexsearch';

type Document<T, Store extends Flexsearch.StoreOption = false> = TDocument<
  T,
  Store
>;
const Document: typeof Flexsearch.Document = (_flexsearch as any).Document;

export type { IndexOptionsForDocumentSearch };
export { Document };
