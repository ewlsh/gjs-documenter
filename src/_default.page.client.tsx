import ReactDOM from 'react-dom';
import { getPage } from 'vite-plugin-ssr/client';
import { Root } from './Root';
import type { PageContext } from './types';
import type { PageContextBuiltInClient } from 'vite-plugin-ssr/client';

hydrate();

async function hydrate() {
  const pageContext = await getPage<
    PageContextBuiltInClient & PageContext<{}>
  >();

  const { Page, pageProps } = pageContext;

  ReactDOM.hydrate(
    <Root pageContext={pageContext}>
      <Page {...pageProps} />
    </Root>,
    document.getElementById('page-view')
  );
}
