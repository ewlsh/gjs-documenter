import React from 'react';
import { PageContextProvider } from './hooks/usePageContext';
import type { PageContext } from './types';

import App from './App';

export { Root };

function Root({
  children,
  pageContext,
}: {
  children: React.ReactNode;
  pageContext: PageContext<any>;
}) {
  return (
    <React.StrictMode>
      <PageContextProvider pageContext={pageContext}>
        <App>{children}</App>
      </PageContextProvider>
    </React.StrictMode>
  );
}
