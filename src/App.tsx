import * as React from 'react';
import './App.scss';

import 'highlight.js/styles/github.css';

import Sidebar from './ui/Sidebar';
import Search from './ui/Search';

import { ServerSide } from './ui/ServerSide';

// @ts-expect-error
import createSharedWorker from './worker?sharedworker';

const App: React.FC = ({ children }) => {
  return (
    <div className="container">
      <nav className="navbar">
        <a className="link" href="/">
          GJS Docs
        </a>

        <Search createWorker={createSharedWorker} />
      </nav>

      <div className="content-wrapper">
        <div className="split">
          <ServerSide>
            <Sidebar />
          </ServerSide>
          <div className="content">{children}</div>
        </div>
      </div>
    </div>
  );
};

export default App;
