declare module "remark-react" {
    var remark: any;
    export = remark;
}

declare module "rehype-highlight" {
    var behead: any;
    export = behead;
}

declare module "remark-behead" {
    var behead: any;
    export = behead;
}

declare module "remark-heading-id" {
    var behead: any;
    export = behead;
}

declare module "react-syntax-highlighter/dist/esm/languages/hljs/c" {
    const language: any;
    export default language;
}