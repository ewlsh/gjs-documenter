import type {
  AliasJson,
  ClassJson,
  FunctionJson,
  NamespaceJson,
  ConstJson,
  InterfaceJson,
  CallbackJson,
  EnumJson,
  RecordJson,
  ErrorJson,
} from '@gi.ts/lib/dist/generators/json';

import { ServerSide } from '../ui/ServerSide';
import { useServersidePageContext } from '../hooks/usePageContext';

import NamespaceWrapper from '../templates/NamespaceWrapper';

import Alias from '../components/Alias';
import Callback from '../components/Callback';
import Class from '../components/Class';
import Constant from '../components/Constant';
import Enum from '../components/Enum';
import Function from '../components/Function';
import Interface from '../components/Interface';

import { PageContext } from '../types';

type Node =
  | ClassJson
  | RecordJson
  | FunctionJson
  | AliasJson
  | ConstJson
  | InterfaceJson
  | CallbackJson
  | EnumJson
  | ErrorJson;

const NodeResolver = ({ node }: { node: Node }) => {
  if (node.kind === 'class' || node.kind === 'record') {
    return <Class node={node} />;
  } else if (node.kind === 'alias') {
    return <Alias node={node} />;
  } else if (node.kind === 'constant') {
    return <Constant node={node} />;
  } else if (node.kind === 'function') {
    return <Function node={node} />;
  } else if (node.kind === 'interface') {
    return <Interface node={node} />;
  } else if (node.kind === 'enum') {
    return <Enum node={node} />;
  } else if (node.kind === 'callback') {
    return <Callback node={node} />;
  } else if (node.kind === 'error') {
    return <Class node={node} />;
  }

  return null;
};

interface Context extends PageContext {
  node: Node;
  namespace: NamespaceJson;
}

const Page = () => {
  const { namespace, node } = useServersidePageContext<Context>();

  return (
    <ServerSide>
      {namespace && node ? (
        <NamespaceWrapper
          namespace={namespace.name}
          version={namespace.version}
          imports={namespace.imports}
        >
          <NodeResolver node={node} />
        </NamespaceWrapper>
      ) : null}
    </ServerSide>
  );
};

export { Page };
