import { ServerSide } from '../ui/ServerSide';
import { useServersidePageContext } from '../hooks/usePageContext';
import NamespaceWrapper from '../templates/NamespaceWrapper';
import type { NamespaceJson } from '@gi.ts/lib/dist/generators/json';
import { NamespaceNode } from '@gi.ts/generator-html/nodes';
import { PageContext } from '../types';

export const passToClient = ['pageProps'];

interface Context extends PageContext {
  node: NamespaceJson;
}

const Page = () => {
  const { node } = useServersidePageContext<Context>();

  return (
    <ServerSide>
      {node ? (
        <NamespaceWrapper
          namespace={node.name}
          version={node.version}
          imports={node.imports}
        >
          <NamespaceNode node={node} />
        </NamespaceWrapper>
      ) : null}
    </ServerSide>
  );
};

export { Page };
