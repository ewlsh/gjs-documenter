// The Home Page
import { NamespaceJson } from '@gi.ts/lib/dist/generators/json';
import { PageContext } from '../types';
import { usePageContext } from '../hooks/usePageContext';

interface Context extends PageContext {
  namespaces: Record<string, NamespaceJson[]>;
}

export default () => {
  const { namespaces } = usePageContext<Context>();

  return (
    <div>
      <h1>It's doc time.</h1>
      <br />
      All Namespaces:
      <ul>
        {Object.entries(namespaces).map(([, post]) =>
          post
            ? post.map((namespace) => (
                <li key={`${namespace.name}-${namespace.version}`}>
                  <a
                    href={`/${namespace.name.toLowerCase()}-${
                      namespace.version
                    }/`}
                  >
                    {namespace.name} {namespace.version}
                  </a>
                </li>
              ))
            : null
        )}
      </ul>
    </div>
  );
};
