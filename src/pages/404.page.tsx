// 404 Page

export default () => (
  <div>
    <h1>Page Not Found</h1>
    <button>
      <a href="/">Go Home</a>
    </button>
  </div>
);
