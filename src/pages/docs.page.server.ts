import { PageContext } from '../types';
import { loadAllNamespaces } from '../data';

export async function onBeforeRender(
  pageContext: PageContext<{}, { slug: string }>
) {
  const allNamespaces = await loadAllNamespaces();

  const node = allNamespaces.find(
    (n) => n.slug === pageContext.routeParams.slug
  );

  if (!node) return null;

  return {
    pageContext: {
      node,
    },
  };
}

export async function prerender() {
  const allNamespaces = await loadAllNamespaces();

  const routes = [
    ...allNamespaces.map((namespace) => {
      let base = `/${namespace.name.toLowerCase()}-${namespace.version}`;

      return [
        {
          url: base,
        },
      ];
    }),
  ]
    .flat(2)
    .map(({ url }) => url);

  return routes;
}
