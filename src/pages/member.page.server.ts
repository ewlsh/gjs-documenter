import type { NamespaceJson } from '@gi.ts/lib/dist/generators/json';

import { PageContext } from '../types';
import { loadAllNamespaces } from '../data';

export const passToClient = ['pageProps'];

function resolveNode(namespace: NamespaceJson, name: string) {
  return (
    namespace.alias.find((node) => node.name === name) ??
    namespace.constants.find((node) => node.name === name) ??
    namespace.classes.find((node) => node.name === name) ??
    namespace.callbacks.find((node) => node.name === name) ??
    namespace.enums.find((node) => node.name === name) ??
    // @ts-ignore
    namespace.errors?.find?.((node) => node.name === name) ??
    namespace.functions.find((node) => node.name === name) ??
    namespace.interfaces.find((node) => node.name === name) ??
    namespace.records.find((node) => node.name === name)
  );
}

export async function onBeforeRender(
  pageContext: PageContext<{}, { slug: string; name: string }>
) {
  const allNamespaces = await loadAllNamespaces();

  const namespace = allNamespaces.find(
    (n) => n.slug === pageContext.routeParams.slug
  );

  if (!namespace) return null;

  const node = resolveNode(namespace, pageContext.routeParams.name);

  return {
    pageContext: {
      namespace: {
        name: namespace.name,
        version: namespace.version,
        imports: namespace.imports,
      },
      node,
    },
  };
}

export async function prerender() {
  const allNamespaces = await loadAllNamespaces();

  const routes = [
    ...allNamespaces.map((namespace) => {
      let base = `/${namespace.name.toLowerCase()}-${namespace.version}`;

      return [
        namespace.functions.map((fn) => ({
          url: `${base}/${fn.name}`,
        })),
        [
          ...namespace.classes,
          ...namespace.records,
          // @ts-ignore
          ...namespace.errors,
        ].map((klass) => ({
          url: `${base}/${klass.name}`,
        })),
        namespace.interfaces.map((klass) => ({
          url: `${base}/${klass.name}`,
        })),
        namespace.callbacks.map((fn) => ({
          url: `${base}/${fn.name}`,
        })),
        namespace.enums.map((en) => ({
          url: `${base}/${en.name}`,
        })),
        namespace.alias.map((alias) => ({
          url: `${base}/${alias.name}`,
        })),
        namespace.constants
          .filter((/** @type {{ doc: any; }} */ c) => c.doc != null)
          .map((constant) => ({
            url: `${base}/${constant.name}`,
          })),
      ];
    }),
  ]
    .flat(2)
    .map(({ url }) => url);

  return routes;
}
