import type { NamespaceJson } from '@gi.ts/lib/dist/generators/json';
import { PageContext } from '../types';
import { usePageContext } from './usePageContext';

interface Context extends PageContext {
  namespaces: Record<string, NamespaceJson[]>;
}

export const useNamespaces = () => {
  const { namespaces = {} } = usePageContext<Context>();

  return namespaces;
};
