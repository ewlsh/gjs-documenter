// `usePageContext` allows us to access `pageContext` in any React component.
// More infos: https://vite-plugin-ssr.com/pageContext-anywhere

import React, { useContext } from 'react';
import type { PageContext } from '../types';

export { PageContextProvider };
export { usePageContext };

const Context = React.createContext<PageContext<{}>>(undefined as any);

function PageContextProvider({
  pageContext,
  children,
}: {
  pageContext: PageContext<{}>;
  children: React.ReactNode;
}) {
  return <Context.Provider value={pageContext}>{children}</Context.Provider>;
}

function usePageContext<Context extends PageContext>() {
  const pageContext = useContext(Context);
  return pageContext as Context;
}

export function useServersidePageContext<
  Context extends PageContext
>(): Partial<Context> {
  const pageContext = usePageContext<Context>();

  if (typeof window === 'undefined') {
    return pageContext;
  } else {
    return {};
  }
}
