import type { ConstJson } from '@gi.ts/lib/dist/generators/json';
import { ConstantNode } from '@gi.ts/generator-html/nodes';

interface ConstantRoute {
  node: ConstJson;
}

export default ({ node }: ConstantRoute) => {
  return <ConstantNode node={node}></ConstantNode>;
};
