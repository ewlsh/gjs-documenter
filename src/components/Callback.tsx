import type { Callback } from '@gi.ts/generator-html/types/nodes';
import { CallbackNode } from '@gi.ts/generator-html/nodes';

interface CallbackRoute {
  node: Callback;
}

export default ({ node }: CallbackRoute) => {
  return <CallbackNode node={node}></CallbackNode>;
};
