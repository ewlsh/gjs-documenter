import type { Function } from '@gi.ts/generator-html/types/nodes';

import { FunctionNode } from '@gi.ts/generator-html/nodes';

interface FunctionRoute {
  node: Function;
}

export default ({ node }: FunctionRoute) => {
  return <FunctionNode node={node}></FunctionNode>;
};
