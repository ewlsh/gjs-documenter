import type { Enum } from '@gi.ts/generator-html/types/nodes';
import { EnumNode } from '@gi.ts/generator-html/nodes';

import {EnumMember} from '@gi.ts/generator-html/components';

interface EnumRoute {
  node: Enum;
}

export default ({ node }: EnumRoute) => {
  return (
    <EnumNode node={node}>
      {node.members?.map((member) => {
         return <EnumMember key={member.name} node={member} />;
      }) ?? []}
    </EnumNode>
  );
};
