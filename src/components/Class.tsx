import type { Class, Record } from '@gi.ts/generator-html/types/nodes';
import { ClassNode } from '@gi.ts/generator-html/nodes';

import './Class.scss';

interface ClassRoute {
  node: Class | Record;
}

export default ({ node }: ClassRoute) => {
  return <ClassNode node={node}></ClassNode>;
};
