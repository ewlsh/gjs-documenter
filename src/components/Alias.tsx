import type { Alias } from '@gi.ts/generator-html/types/nodes';
import { AliasNode } from '@gi.ts/generator-html/nodes';

interface AliasRoute {
  node: Alias;
}

export default ({ node }: AliasRoute) => {
  return <AliasNode node={node}></AliasNode>;
};
