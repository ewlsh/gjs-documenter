import type { Interface } from '@gi.ts/generator-html/types/nodes';
import { InterfaceNode } from '@gi.ts/generator-html/nodes';

interface InterfaceRoute {
  node: Interface;
}

export default ({ node }: InterfaceRoute) => {
  return <InterfaceNode node={node}></InterfaceNode>;
};
