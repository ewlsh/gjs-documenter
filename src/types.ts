// The `pageContext` that are available in both on the server-side and browser-side
export interface PageContext<
  Props extends {} = {},
  RouteParams extends {} = {}
> {
  Page: (pageProps: Props) => React.ReactElement;
  pageProps: Props;
  routeParams: RouteParams;
  urlPathname: string;
  documentProps?: {
    title?: string;
    description?: string;
  };
}

export interface ServerPageContext<
  Props extends {} = {},
  RouteParams extends {} = {}
> extends PageContext<Props, RouteParams> {
  runOnBeforeRenderPageHook<
    T extends ServerPageContext<{}, {}> = ServerPageContext<{}, {}>
  >(
    pageContext: PageContext<Props, RouteParams>
  ): { pageContext: T };
}
