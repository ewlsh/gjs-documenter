/// <reference no-default-lib="true" />
/// <reference lib="webworker" />

import * as flexsearch from './flexsearch';

import debounce from 'debounce';

import { toDocuments, defaultSearchOptions, DocumentType } from './search';

const worker: SharedWorkerGlobalScope = globalThis[
  'self'
] as unknown as SharedWorkerGlobalScope;

let document: flexsearch.Document<DocumentType, string[]> | null = null;

let loaded = false;

async function loadDocument() {
  if (loaded) return;

  document = new flexsearch.Document<DocumentType, string[]>({
    ...defaultSearchOptions,
  });

  loaded = true;

  const response = await fetch('/search.json');
  const { '::documents': rawDocuments, ...searchDocument } =
    await response.json();

  const documents = toDocuments(JSON.parse(rawDocuments));

  documents.forEach((object) => {
    document.add(object.path, object);
  });

  await Promise.all(
    Object.entries(searchDocument).map(([key, data]) => {
      // @ts-expect-error Incorrect type
      return document.import(key.replace(/-/g, ':'), data);
    })
  ).then(() => {
    console.log('Search imported.');
  });

  return document;
}

const listener = debounce(function (this: MessagePort, ev) {
  var search = ev.data;

  console.log('search: ' + ev.data);

  const fieldResults = document.search(search, 10, {
    limit: 10,
    enrich: true,
  });

  const results = fieldResults
    .map((fieldResult) => {
      console.log(fieldResult);
      return [fieldResult.field, fieldResult.result] as const;
    })
    .map(([field, results]) => {
      return results.map((result) => ({
        ...result.doc,
        id: field + '(' + result.id + ')',
        path: result.id,
      }));
    })
    .flat();
  const keys = new Set();
  const dedup = results.reduce((prev, next) => {
    if (keys.has(next.id)) return prev;

    prev.push(next);
    keys.add(next.id);

    return prev;
  }, []);
  console.log(dedup);
  this.postMessage(dedup);
  console.log('Search complete...');
}, 300);

worker.onconnect = function (event) {
  console.log(event);
  const [port] = event.ports;

  loadDocument()
    .then(() => {
      console.log('Connecting...');
      port.addEventListener('message', listener);
      port.addEventListener('messageerror', (error) => console.log(error));
      port.start();
    })
    .catch((error) => {
      console.error(error);

      port.removeEventListener('message', listener);
    });
};
