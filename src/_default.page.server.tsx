import ReactDOMServer from 'react-dom/server';

import { Root } from './Root';
import { escapeInject, dangerouslySkipEscape } from 'vite-plugin-ssr';

import type { PageContext, ServerPageContext } from './types';

import { loadAllNamespaces, updateSearchData } from './data';

export { render, onBeforeRender };

export const passToClient = ['pageProps'];

async function onBeforeRender(pageContext: ServerPageContext) {
  const { pageContext: pageContextAddendum } =
    await pageContext.runOnBeforeRenderPageHook(pageContext);

  const allNamespaces = await loadAllNamespaces();

  // Ensure search data is updated
  await updateSearchData();

  const namespaces = allNamespaces.reduce((dict, namespace) => {
    if (!dict[namespace.name]) dict[namespace.name] = [];

    dict[namespace.name].push({
      name: namespace.name,
      version: namespace.version,
      alias: namespace.alias ?? [],
      imports: { ...namespace.imports },
    });

    return dict;
  }, {} as Record<string, any>);

  return {
    pageContext: {
      namespaces,
      ...pageContextAddendum,
    },
  };
}

async function render(pageContext: PageContext<{}>) {
  const { Page, pageProps } = pageContext;
  const pageHtml = ReactDOMServer.renderToString(
    <Root pageContext={pageContext}>
      <Page {...pageProps} />
    </Root>
  );

  // See https://vite-plugin-ssr.com/html-head
  const { documentProps } = pageContext;
  const title = (documentProps && documentProps.title) || 'Vite SSR app';
  const desc =
    (documentProps && documentProps.description) ||
    'App using Vite + vite-plugin-ssr';

  const documentHtml = escapeInject`<!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="${desc}" />
        <title>${title}</title>
      </head>
      <body>
        <div id="page-view">${dangerouslySkipEscape(pageHtml)}</div>
      </body>
    </html>`;

  return {
    documentHtml,
    pageContext: {
      // We can add some `pageContext` here, which is useful if we want to do page redirection https://vite-plugin-ssr.com/page-redirection
    },
  };
}
