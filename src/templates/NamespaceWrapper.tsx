import React from 'react';

import {
  NamespaceContext,
  PathFormat,
  NamedNode,
} from '@gi.ts/generator-html/path';

import { LinkContext } from '@gi.ts/generator-html/path';

interface NamespaceProps {
  namespace: string; //Namespace;
  version: string;
  imports: Record<string, string>;
}

const NamespaceWrapper: React.FC<NamespaceProps> = ({
  version,
  imports,
  namespace,
  children,
}) => {
  function getPath(node: NamedNode) {
    return `/${namespace.toLowerCase()}-${version.toLowerCase()}/${node.name}`;
  }

  function getNamespaceVersion(_namespace: string) {
    return '404';
  }

  return (
    <LinkContext.Provider
      value={({ to, children }) => <a href={to}>{children}</a>}
    >
      <NamespaceContext.Provider
        value={{
          getPath,
          getNamespaceVersion,
          meta: {
            namespace,
            version,
            imports,
          },
          pathFormat: PathFormat.NAME_VERSION,
        }}
      >
        {children}
      </NamespaceContext.Provider>
    </LinkContext.Provider>
  );
};

export default NamespaceWrapper;
