import type * as Flexsearch from '../node_modules/@types/flexsearch/index';

export const defaultSearchOptions: Flexsearch.IndexOptionsForDocumentSearch<
  unknown,
  string[]
> = {
  context: true,
  document: {
    id: 'path',
    tag: 'type',
    index: ['name', 'children[]'],
    // ["classes[]", "interfaces[]", "records[]", "constants[]", "functions[]", "callbacks[]", "errors[]", "enums[]", "alias[]"]
    store: ['name', 'namespace', 'version', 'children[]'],
  },
};

enum Keys {
  name = 0,
  path,
  version,
  namespace,
  functions,
  class,
  constant,
  documents,
  children,
}

interface NamespaceDeoptimizedObject {
  name: string;
  version: string;
  path: string;
  documents: {
    functions: {
      name: string;
      path: string;
    }[];
    class: {
      name: string;
      path: string;
      children: string[];
    }[];
    constant: {
      name: string;
      path: string;
    }[];
  };
}

type MapToKeys<T extends Record<string, any>> = {
  [key in keyof T as key extends keyof typeof Keys
    ? typeof Keys[key] extends number
      ? typeof Keys[key]
      : never
    : never]: T[key] extends any[]
    ? MapToKeys<T[key][number]>[]
    : T[key] extends Record<string, any>
    ? MapToKeys<T[key]>
    : T[key];
};

type NamespaceOptimizedObject = MapToKeys<NamespaceDeoptimizedObject>;

export function optimize(
  objects: NamespaceDeoptimizedObject[]
): NamespaceOptimizedObject[] {
  return objects.map(({ documents, ...object }) => {
    return {
      [Keys.namespace]: object.name,
      [Keys.name]: object.name,
      [Keys.version]: object.version,
      [Keys.path]: object.path,
      [Keys.documents]: {
        [Keys.constant]: documents.constant.map((doc) => {
          return {
            [Keys.name]: doc.name,
            [Keys.path]: doc.path,
          };
        }),
        [Keys.functions]: documents.functions.map((doc) => {
          return {
            [Keys.name]: doc.name,
            [Keys.path]: doc.path,
          };
        }),
        [Keys.class]: documents.class.map((doc) => {
          return {
            [Keys.name]: doc.name,
            [Keys.path]: doc.path,
            [Keys.children]: doc.children,
          };
        }),
      },
    };
  });
}

export interface DocumentType {
  namespace: string;
  type: string;
  name: string;
  version: string;
  path: string;
  children?: string[];
}

export function toDocuments(objects: NamespaceOptimizedObject[]) {
  return objects
    .map(({ [Keys.documents]: documents, ...object }) => {
      return [
        {
          namespace: object[Keys.name],
          type: 'namespace',
          name: object[Keys.name],
          version: object[Keys.version],
          path: object[Keys.path],
        },
        ...documents[Keys.constant].map((doc) => {
          return {
            namespace: object[Keys.name],
            version: object[Keys.version],
            path: object[Keys.path] + doc[Keys.path],
            name: doc[Keys.name],
            type: 'constant',
          };
        }),
        ...documents[Keys.functions].map((doc) => {
          return {
            namespace: object[Keys.name],
            version: object[Keys.version],
            path: object[Keys.path] + doc[Keys.path],
            name: doc[Keys.name],
            type: 'function',
          };
        }),
        ...documents[Keys.class].map((doc) => {
          return {
            namespace: object[Keys.name],
            version: object[Keys.version],
            path: object[Keys.path] + doc[Keys.path],
            name: doc[Keys.name],
            children: doc[Keys.children],
            type: 'class',
          };
        }),
      ];
    })
    .flat(2);
}
