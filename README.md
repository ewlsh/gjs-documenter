# GJS Experimental Docs 

An experimental renderer for GJS documentation, based on [gi.ts](https://gitlab.gnome.org/ewlsh/gi.ts)'s `html-renderer`

## Setup

```sh
yarn
yarn config-docs # Generate list of local docs
yarn list-docs # List available docs
yarn gen-docs # Generate documentation
```

## Develop Site

```sh
yarn start
```

## Generate Site

```sh
# This compiles the site's styles and JS
yarn build
# This generates every HTML page of documentation for the site to display
yarn prerender
```


## Publish

```sh
# Uploads the updated files to Firebase hosting
yarn publish
```