// @ts-check

import * as flexsearch from 'flexsearch';

/**
 * @type {flexsearch.IndexOptionsForDocumentSearch<{}, string[]>}
 */
const defaultSearchOptions = {
    context: true,
    document: {
        id: 'path',
        tag: 'type',
        index: [
            "name",
            "children[]"
        ],
        // ["classes[]", "interfaces[]", "records[]", "constants[]", "functions[]", "callbacks[]", "errors[]", "enums[]", "alias[]"]
        store: [
            "name",
            "namespace",
            "version",
            "children[]",
        ],
    }
};

export default defaultSearchOptions;