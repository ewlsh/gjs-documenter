import react from '@vitejs/plugin-react';
import ssr from 'vite-plugin-ssr/plugin';
import { defineConfig } from 'vite';

const config = defineConfig({
  resolve: {
    alias: {},
  },
  server: {
    fs: {
      strict: false,
    },
  },
  json: {
    stringify: true,
  },
  plugins: [react(), ssr()],
  // @ts-expect-error
  ssr: {
    noExternal: ['uuid'],
    external: ['react'],
  },
  optimizeDeps: {
    exclude: ['replace-ext'],
  },
});

export default config;
